
var socket = io();
window.onload = function(){
  var canvas = document.getElementsByTagName('canvas')[0].getContext('2d'),
  click = false;


  $(window).mousedown(function(){
    click = true;
  });

  $(window).mouseup(function(){
    click = false;
  });

  $('canvas').mousedown(function(e){
   var obj = {
      pageX: e.pageX,
      pageY: e.pageY,
      color: changeColor(),
      width: changeLineWidth(),
    };
    socket.emit('canvasMouseDown', obj );
    draw(obj);
  });

  $('canvas').mouseup(function(e){
    var obj = {
      pageX: e.pageX,
      pageY: e.pageY,
      color: changeColor(),
      width: changeLineWidth(),
    }

    socket.emit('canvasMouseUp', obj);
    draw(obj);
  });

  $('canvas').mousemove(function(e){
   var obj = {
      pageX: e.pageX,
      pageY: e.pageY,
      color: changeColor(),
      width: changeLineWidth(),
    }
    if(click === true){
      socket.emit('canvasMouseMove', obj);
      draw(obj);
    }
  });

  function changeColor(){
    var color = $('input[type=color]').val();
      socket.on( 'changeColor', color );
    return color;
  }
 function changeLineWidth(){
    var width = $('input[type=range]').val();
      socket.on( 'changeLineWidth', width );
    return width;
  }

  function draw(obj){

    // canvas.strokeStyle = obj.color;

    // canvas.lineWidth = obj.width;

    // canvas.lineCap = 'round';

    // canvas.clearRect(0, 0, 700, 500);

    // canvas.beginPath();

    // canvas.moveTo(this.x1, this.y1);

    // canvas.lineTo(this.x, this.y);

    // canvas.stroke();


    canvas.beginPath();
    canvas.fillStyle = obj.color;
    canvas.arc(obj.pageX - $('canvas').offset().left,
               obj.pageY - $('canvas').offset().top,
               obj.width, 0, 2 * Math.PI);
    canvas.fill();
    canvas.closePath();
    requestAnimationFrame(draw);
  }

  socket.on('canvasMouseDown', function(data){
    draw(data);
  });
  socket.on('canvasMouseUp', function(data){
    draw(data);
  });
  socket.on('canvasMouseMove', function(data){
    draw(data);
  });
}
