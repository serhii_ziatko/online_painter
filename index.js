
//Setup basic express server
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;

server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

//Routing
app.use(express.static(__dirname + '/public'));


io.on('connection', function (socket) {

  socket.on('canvasMouseDown', function (data) {
    socket.broadcast.emit('canvasMouseDown', data);
  });

  socket.on('canvasMouseUp', function (data) {
    socket.broadcast.emit('canvasMouseUp', data);
  });

  socket.on('canvasMouseMove', function (data) {
    socket.broadcast.emit('canvasMouseMove', data);
  });

  socket.on('changeColor', function(data){
    socket.emit('changeColor', data);
    socket.broadcast.emit('changeColor', data);
  })

});
